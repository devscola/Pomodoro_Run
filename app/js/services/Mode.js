const ZERO_SECONDS = 0
const TO_MILLISECONDS = 60 * 1000

class Mode {
  constructor(bus, client){
    this.bus = bus
    this.client = client
    this.numberOfMinutes = null
    this.isPaused = true

    this.bus.subscribe('timer.selectMode', this.setModeUp.bind(this))
  }

  setModeUp(data){
    this.numberOfMinutes = data.time
    this.client.postJson(data.route, data.time)
    this.bus.publish('timer.selectedMode', this._message())
  }

  _message() {
    return {
      minutes: this.numberOfMinutes,
      seconds: ZERO_SECONDS,
      isPaused: this.isPaused,
      initialTime: this.numberOfMinutes * TO_MILLISECONDS
    }
  }
}

export default Mode