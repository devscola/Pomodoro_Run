class Translations {
    static retrieve(){
       return   {
            start:'Inicio',
            break:'Descanso',
            longBreak: 'Descanso largo',
            reset:'Reinicio',
            synchro: 'Sincro',
            retro: 'Retro',
            numberOfPomodoros:'Número de Pomodoros',
            pushNotificationPomodoro:'Has terminado el Pomodoro. Tómate un descanso',
            pushNotificationBreak: 'Tu descanso ha terminado. Vuelve al trabajo',

        }
    }
}
export default Translations
