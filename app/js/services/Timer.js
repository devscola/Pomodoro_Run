const NO_TIME_LEFT = 0
const INITIAL_COUNTER = 0
const RETRO_TIME_IN_MINUTES = 60
const INITIAL_TIME = 25 * 60 * 1000
const RETRO_TIME = 60 * 60 * 1000

class Timer {

    constructor(bus, time,client) {
    this.time = time
    this.bus = bus
    this.client = client

    this.timeLeft = INITIAL_TIME
    this.isPaused = false

    this.counterPomodoro = INITIAL_COUNTER

    this._subscribe()
    this._initialPomodoro()
  }

  _subscribe() {
    this.bus.subscribe('timer.start', this.start.bind(this))
    this.bus.subscribe('timer.askTimeLeft', this.calculateTimeLeft.bind(this))
    this.bus.subscribe('retrieved.timeLeft', this.declareTimeLeft.bind(this))
    this.bus.subscribe('timer.selectedMode', this.stablishState.bind(this))
    this.bus.subscribe('timer.reseted', this.stablishState.bind(this))
  }

  stablishState(message){
    this.isPaused = message.isPaused
    this.timeLeft = message.initialTime
  }

  _initialPomodoro(){
    this.client.postJson('initializeTime', INITIAL_TIME)
  }

  _retrieveTimeLeft(){
    const callback = this._buildCallback('retrieved.timeLeft')
    // this.client.getJson('getRemainingTime', callback)
    this.client.getJson('timeLeft', callback)
  }

  _buildCallback(message) {
    return (response) => {
      const time = response.time
      this.bus.publish(message, time)
    }
  }

  declareTimeLeft(time){
    this.timeLeft = Number(time)
  }

  start() {
    this.isPaused = false
    this._retrieveTimeLeft()
    this.bus.publish('timer.started', this._message())
  }

  calculateTimeLeft() {
    if(!this.isPaused) {
      this._retrieveTimeLeft()
      // this.timeLeft = this.timeLeft - 1000
    }

    if(!this._hasTimeLeft()) {
      this._putTimeToZero()
      this._addOneToCounter()

      this.bus.publish('timer.end', this._message())
      this._changeToInitialState()

    }else{
      this.bus.publish('timer.timeLeft', this._message())
    }
  }

  _message() {
    let minutes = this._toVisulize60()
    return {
      minutes: minutes,
      seconds:this.time.toSeconds(this.timeLeft),
      counter: this.counterPomodoro,
    }
  }

  _hasTimeLeft() {
    return (this.timeLeft >= NO_TIME_LEFT)
  }

  _changeToInitialState(){
    this.timeLeft = INITIAL_TIME
  }

  _putTimeToZero() {
    this.timeLeft = NO_TIME_LEFT
    this.alarm()
  }

  _toVisulize60(){
    let minutes = null
    if(this.timeLeft === RETRO_TIME){
      minutes = RETRO_TIME_IN_MINUTES
    }else {
      minutes = this.time.toMinutes(this.timeLeft)
    }
    return minutes
  }

  _addOneToCounter(){
    if(this._workPomodoro()) {
      return this.counterPomodoro++
    }
  }

  _workPomodoro(){
    return !this.isAtBreak && !this.isAtRetro && !this.isAtSynchro && !this.isAtLongBreak
  }

  alarm() {
    //let audio = new Audio('/alarm_songs/R2D2_Audio.mp3')
    //audio.play()
  }
}

export default Timer
