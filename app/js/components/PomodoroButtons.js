import Buttons from '../views/Buttons.js'
import State from './State.js'

const SYNCHRO_COLOR = '#8e50ed'
const RETRO_COLOR = '#3541D4'
const DEFAULT_COLOR = '#479eeb'

class PomodoroButtons {
  constructor(container, bus, document, background) {
    this.state = new State
    this.container = container
    this.renderer = new Buttons(document)
    this.bus = bus
    this.containerBackground = background
    this.nonInitiated = true
    this._subscribe()
  }

  _subscribe() {
    this.bus.subscribe('get.translations', this.renderTranslate.bind(this))
    this.bus.subscribe('timer.started', this._startButton.bind(this))
    this.bus.subscribe('timer.end', this._end.bind(this))
  }

  draw() {
    this.bus.publish('got.translations')
    this._drawRenderer(this.state)
  }

  reset() {
    this.bus.publish('timer.selectMode',{time: 25, route:'reset'} )
    this._switchButton(true, DEFAULT_COLOR)
  }

  break() {
    this.bus.publish('timer.selectMode',{time: 5, route:'break'} )
    this._switchButton(false, DEFAULT_COLOR)
  }

  longBreak() {
    this.bus.publish('timer.selectMode', {time: 15, route:'longBreak'})
    this._switchButton(false, DEFAULT_COLOR)
  }

  synchro() {
    this.bus.publish('timer.selectMode', {time: 45, route:'synchro'})
    this._switchButton(false, SYNCHRO_COLOR)
  }

  retro() {
    this.bus.publish('timer.selectMode', {time: 60, route:'retro'})
    this._switchButton(false, RETRO_COLOR)
  }

  _start() {
    this.bus.publish('timer.start')
    this._startButton()
  }

  renderTranslate(collection){
    this.collection = collection
    this.renderer.drawTranslations(this.collection)
  }

  _startButton() {
    if(this.nonInitiated === true){
      this.state.startMode()
    }else{
      this.state.runNonstopCountdown()
    }
    this.draw()
  }

  _switchButton(initiated, backgroundColor){
    this.nonInitiated = initiated
    this.state.prepareMode()
    this._changeBackgroundColor(backgroundColor)
    this.draw()
  }

  _changeBackgroundColor(color) {
    console.log(this.containerBackground.style)
    this.containerBackground.style.background = color
  }

  _end(){
    this.nonInitiated = false
    this.state.initialStatus()

    this.draw()
  }

  _drawRenderer(message) {
    const callbacks = {
      startCountDown: this._start.bind(this),
      reset: this.reset.bind(this),
      break: this.break.bind(this),
      longBreak: this.longBreak.bind(this),
      synchro: this.synchro.bind(this),
      retro: this.retro.bind(this)
    }

    this.container.innerHTML = this.renderer.render(message)
    this.renderer.addCallbacks(callbacks)
  }
}

export default PomodoroButtons
