class ApiRequest {

    static async getJson(route, callback) {
        const api_url = `${process.env.API_URL}/${route}`
        let response = await fetch(api_url)
        const data = await response.json()
        callback(data)
    }

    static async postJson(route, payload) {
        let postObject = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ time: payload })
        }
        const api_url = `${process.env.API_URL}/${route}`
        await fetch(api_url, postObject)
    }
}
export default ApiRequest
