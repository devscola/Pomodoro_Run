import PomodoroButtons from './components/PomodoroButtons.js'
import PomodoroTimer from './components/PomodoroTimer.js'
import PomodoroCounter from './components/PomodoroCounter.js'
import TranslateService from './services/TranslateService.js'
import PushNotification from './components/PushNotification.js'
import client from './infrastructure/ApiRequest.js'
import Mode from './services/Mode.js'

import PomodoroService from './services/Timer.js'
import Time from './libraries/Time.js'
import Bus from './infrastructure/Bus.js'
import push from 'push.js'

const time = new Time(Date)
const bus = new Bus()
const background = document.querySelector('#background')

const containerButtons = document.querySelector('#pomodoro-buttons')
const containerTimer = document.querySelector('#pomodoro-timer')
const containerCounter = document.querySelector('#pomodoro-counter')

new PomodoroService(bus, time, client)

new TranslateService(bus)
new PushNotification(bus, push)
new Mode (bus, client)

const pomodoroButtons = new PomodoroButtons(containerButtons, bus, document, background)
pomodoroButtons.draw()

const pomodoroTimer = new PomodoroTimer(containerTimer, bus, document)
pomodoroTimer.draw()

const pomodoroCounter = new PomodoroCounter(containerCounter, bus, document)
pomodoroCounter.draw()
