class Callback {
  constructor(bus, topic) {
    this.wasCalled = false
    this.payload = null

    bus.subscribe(topic, this.storeInformation.bind(this))
  }

  storeInformation(payload) {
    this.wasCalled = true
    this.payload = payload
  }

  hasBeenCalledWith() {
    return this.payload
  }

  hasBeenCalled() {
    return this.wasCalled
  }
}

export default Callback
