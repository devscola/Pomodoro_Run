import Timer from '../../js/services/Timer.js'
import Time from '../../js/libraries/Time.js'
import Bus from '../../js/infrastructure/Bus.js'
import Callback from '../Callback.js'
import StubDate from '../StubDate.js'
import FakeApiRequest from '../FakeApiRequest.js'


describe('Timer service', () => {
  let bus, time, date, client = null

  beforeEach(() => {
    bus = new Bus()
    date = new StubDate()
    time = new Time(date)
    client = new FakeApiRequest()
  })

  it('subscribes to start event', () => {
    spyOn(bus, 'subscribe')

    new Timer(bus, time, client)

    expect(bus.subscribe).toHaveBeenCalledWith('timer.start', aCallback())
  })

  it('does not send a negative time left', () => {
    const callback = new Callback(bus, 'timer.end')
    const service = new Timer(bus, time, client)
    const negativeNumber = -1
    service.timeLeft = negativeNumber
    const zeroTimeLeft = {
      minutes: 0,
      seconds: 0,
      counter: 1
    }

    service.calculateTimeLeft()

    expect(callback.hasBeenCalledWith()).toEqual(zeroTimeLeft)
  })

  describe('Counter', () => {

    it('when pomodoro starts the counter is 0', () => {
      const count = 0
      const service = new Timer(bus, time, client)
      const result = service.counterPomodoro

      expect(result).toEqual(count)
    })

    it('When timer finish add one to counter', () => {
      const count = 1
      const service = new Timer(bus, time, client)

      service.isAtBreak = false
      service.isAtLongBreak = false
      service.isAtSynchro = false
      service.isAtRetro = false,

      service._addOneToCounter()

      const result = service.counterPomodoro

      expect(result).toEqual(count)
    })

    it('At break, when timer finish dont add one to counter', () => {
      const count = 0
      const service = new Timer(bus, time, client)

      service.isAtBreak = true

      service._addOneToCounter()

      const result = service.counterPomodoro

      expect(result).toEqual(count)
    })

    it('At long break, when timer finish dont add one to counter', () => {
      const count = 0
      const service = new Timer(bus, time, client)

      service.isAtLongBreak = true

      service._addOneToCounter()

      const result = service.counterPomodoro

      expect(result).toEqual(count)
    })

    it('At long break, when timer finish dont add one to counter', () => {
      const count = 0
      const service = new Timer(bus, time, client)

      service.isAtSynchro = true

      service._addOneToCounter()

      const result = service.counterPomodoro

      expect(result).toEqual(count)
    })
  })

  describe('Alarm', () => {
    it(' reproduces the alarm sound after the time ends', () => {
      const service = new Timer(bus, time, client)
      spyOn(service, 'alarm')

      service._putTimeToZero()

      expect(service.alarm).toHaveBeenCalled()
    })
  })

  function aCallback() {
    return jasmine.any(Function)
  }
})
