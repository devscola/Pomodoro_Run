import Bus from '../../js/infrastructure/Bus.js'
import Callback from '../Callback.js'
import FakeApiRequest from '../FakeApiRequest.js'
import Mode from '../../js/services/Mode.js'

describe('Select Mode', () => {
  let bus, client, mode = null

  beforeEach(() => {
    bus = new Bus()
    client = new FakeApiRequest()
    mode = new Mode(bus, client)
  })
  
  it('on long break, publishes long break event with initial time left', () => {
    const callback = new Callback(bus, 'timer.selectedMode')
    const data = {time: 15, route:'longBreak'}
   
    const initialTimeLeftLongBreak = {
      minutes: 15,
      seconds: 0,
      isPaused: true,
      initialTime: 15 * 60 * 1000
    }
    
    mode.setModeUp(data)

    expect(callback.hasBeenCalledWith()).toEqual(initialTimeLeftLongBreak)
  })
  it('on break, publishes long break event with initial time left', () => {
    const callback = new Callback(bus, 'timer.selectedMode')
    const data = {time: 5, route:'break'}
   
    const initialTimeLeftBreak = {
      minutes: 5,
      seconds: 0,
      isPaused: true,
      initialTime: 5 * 60 * 1000
    }
    
    mode.setModeUp(data)

    expect(callback.hasBeenCalledWith()).toEqual(initialTimeLeftBreak)
  })

  it('on synchro, publishes synchro event with initial time left', () => {
    const callback = new Callback(bus, 'timer.selectedMode')
    const data = {time: 45, route:'synchro'}
   
    const initialTimeLeftSynchro = {
      minutes: 45,
      seconds: 0,
      isPaused: true,
      initialTime: 45 * 60 * 1000
    }
    
    mode.setModeUp(data)

    expect(callback.hasBeenCalledWith()).toEqual(initialTimeLeftSynchro)
  })

  it('on retro, publishes retro event with initial time left', () => {
    const callback = new Callback(bus, 'timer.selectedMode')
    const data = {time: 60, route:'retro'}
   
    const initialTimeLeftRetro = {
      minutes: 60,
      seconds: 0,
      isPaused: true,
      initialTime: 60 * 60 * 1000
    }
    
    mode.setModeUp(data)

    expect(callback.hasBeenCalledWith()).toEqual(initialTimeLeftRetro)
  })

})
