# Pomodoro Devscola

Project originally created as a tool to learn Javascript for the "Run FZBZ" attendants at Devscola. 

The final goal is to provide an app where users can share the same pomodoro session. 

## System requirements

  - Firefox version 60 or higher
  - docker-compose (for Mac install docker app)

## How to run the application

In your terminal type the following commands:
  - `docker-compose up --build`

Copy  the localhost server that is now written in your console and paste it in your browser.
  
  

## How to run the unitary tests

Test will run in 'localhost:8081/tests.html'

## Running all the tests

After running the docker, execute:

  - `sh run-all-tests.sh`

## How to run the integrity tests (requires Cypress installation)
In your console type the following command:
  - npm run test-all (in order to launch tests in terminal).
  - node_modules/.bin/cypress open (in order to access Cypress app).
  - node_modules/.bin/cypress run (in order to run the tests).

## Deploying to Demo

Currently, we have a machine in Heroku to deploy the application for demo purposes, you can check it out at:

  -[APP]  https://app-squadpomodoro.herokuapp.com/
  -[API]  https://api-squadpomodoro.herokuapp.com

## Environment vars 

  We use environment variables to deploy in heroku.
  - for heroku and CI
    $HEROKU_PRODUCTION_API_KEY 
    $API_URL=http://localhost:8081