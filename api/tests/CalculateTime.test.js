const CalculateTime = require('../src/CalculateTime.js')

describe('CalculateTime',()=>{

  it('Initialize time ',()=>{
    const calculateTime = new CalculateTime()
    const breakTime = 5 * 60 * 1000

    calculateTime.initializeTime(breakTime)
    const result = calculateTime.getInitialTime()

    expect(result).toEqual(breakTime)
  })

  it('discounts a second',()=>{
    const calculate = new CalculateTime()
    const timeAfterASecond = {time: '1499000'}

    const result = calculate.sendCurrentTime()

    expect(result).toEqual(timeAfterASecond)
  })

  it('resets Pomodoro with default initial time',()=>{
    const calculate = new CalculateTime()
    const timeAfterReset = 1500000

    calculate.sendCurrentTime()
    calculate.reset()
    const result = calculate.getInitialTime()

    expect(result).toEqual(timeAfterReset)
  })

  it('sets initial break time',()=> {
    const calculate = new CalculateTime()
    const timeAfterBreak = 5 * 60 * 1000

    calculate.breakMode()
    const result = calculate.getInitialTime()

    expect(result).toEqual(timeAfterBreak)
  })
  it('sets initial long break time',()=> {
    const calculate = new CalculateTime()
    const timeAfterLongBreak = 15 * 60 * 1000

    calculate.longBreakMode()
    const result = calculate.getInitialTime()

    expect(result).toEqual(timeAfterLongBreak)
  })

  it('sets initial synchro time',()=> {
    const calculate = new CalculateTime()
    const timeAfterSynchro = 45 * 60 * 1000

    calculate.synchroMode()
    const result = calculate.getInitialTime()

    expect(result).toEqual(timeAfterSynchro)
  })

  it('sets initial  retro time',()=> {
    const calculate = new CalculateTime()
    const timeAfterRetro = 60 * 60 * 1000

    calculate.retroMode()
    const result = calculate.getInitialTime()

    expect(result).toEqual(timeAfterRetro)
  })

  xit('doesnt discount a second if its already running',()=>{
    const calculate = new CalculateTime()
    const timeAfterASecond = {time: '1499000'}

    const result = calculate.sendCurrentTime()

    expect(result).toEqual(timeAfterASecond)
  })
})
