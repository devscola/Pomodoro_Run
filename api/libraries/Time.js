class Time {
  constructor(time) {
    this.time = time
  }

  currentMoment() {
    const inMilliseconds = this.time.now()
    return inMilliseconds
  }

  toSeconds(milliseconds){
    let seconds = new Date(milliseconds)
    return seconds.getSeconds()    
  }

  toMinutes(milliseconds) {
    let minutes = new Date(milliseconds)
    return minutes.getMinutes()


  }
}

module.exports = Time
