const INITIAL_POMODORO = 25 * 60 * 1000
const ONE_SECOND = 1000
const BREAK_TIME = 5 * 60 * 1000
const LONG_BREAK_TIME = 15 * 60 * 1000
const SYNCHRO_TIME = 45 * 60 * 1000
const RETRO_TIME = 60 * 60 * 1000

class CalculateTime{
  constructor (){
    this.currentTime = INITIAL_POMODORO
  }

  sendCurrentTime(){
    this.currentTime -= ONE_SECOND
    return {time:`${this.currentTime}`}
  }

  initializeTime(time){
    this.currentTime = time
  }

  reset(){
    this.currentTime = INITIAL_POMODORO
  }

  getInitialTime(){
    return this.currentTime
  }

  breakMode(){
    this.currentTime = BREAK_TIME
  }

  longBreakMode(){
    this.currentTime =  LONG_BREAK_TIME
  }

  synchroMode(){
    this.currentTime = SYNCHRO_TIME
  }

  retroMode(){
    this.currentTime = RETRO_TIME
  }
}


module.exports = CalculateTime
