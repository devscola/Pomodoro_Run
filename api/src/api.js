const express = require('express')
const api = express()
const cors = require('cors')
const port = process.env.PORT || 8081

const CalculateTime = require('./CalculateTime.js')
const calculate = new CalculateTime()

api.use(cors())
api.use(express.json())
api.options('*', cors())

api.post('/break', (request, response) => {
    calculate.breakMode()
})

api.post('/longBreak', (request, response) => {
    calculate.longBreakMode()
})

api.post('/synchro', (request, response) => {
    calculate.synchroMode()
})

api.post('/retro', (request, response) => {
    calculate.retroMode()
})

api.post('/initializeTime', (request, response) => {
    let time = request.body.time
    calculate.initializeTime(time)
})

// api.get('/getRemainingTime', (request, response) => {
//     const remainingTime = calculate.getRemainingTime()
//     response.json(remainingTime)
// })

api.get('/timeLeft', (request, response) => {
    const currentTime = calculate.sendCurrentTime()
    response.json(currentTime)
})

api.post('/reset', (request, response) => {
    calculate.reset()
})

api.listen(port, () => console.log(`Almuerzo en la china Premium  ${port}!`))
