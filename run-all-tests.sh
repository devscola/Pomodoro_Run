#/bin/sh

docker-compose exec -T app npm test
docker-compose exec -T api npm test
docker run --network="host" -v $(pwd)/e2e:/workdir rdelafuente/cypress
